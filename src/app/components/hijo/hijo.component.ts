import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {
  mensajeH = 'Mensaje desde el descendientel';

  constructor() { }

  ngOnInit(): void {
  }

}
